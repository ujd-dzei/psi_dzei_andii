sum(0, X, X).
sum(X, 0, X).
sum(X, 1, X+1).
sum(X, Y, Z) :- sum(X, Y1, Z1), Y is Y1+1, Z is Z1+1.

