jest_matką(X, Y) :- jest_synem(Y, X); jest_córką(Y, X).
jest_synem(X, Y) :- jest_matką(Y, X); jest_ojcem(Y, X).
brat(X, Y) :- jest_matką(Z, Y), jest_matką(Z, X); jest_ojcem(Z, Y), jest_ojcem(Z, X).
babcia(X, Y) :- jest_matką(X, Z), jest_matką(Z, Y); jest_matką(X, Z), jest_ojcem(Z, Y).

