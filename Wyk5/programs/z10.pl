mul(0, _, 0).
mul(1, X, X).
mul(X, Y, Z) :- X > 1, X1 is X - 1, mul(X1, Y, Z1), Z is Z1 + Y.

pot(_, 0, 1).
pot(X, 1, X).
pot(X, Y, Z) :- pot(X, Y1, Z1), Y is Y1+1, mul(X, Z1, Z).
